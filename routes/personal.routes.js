const express = require('express');
const mongoose = require('mongoose');

const PersonalData = require('../models/PersonalData');


const router = express.Router();

router.get('/', async (req, res, next) => {
    const { viewAll } = req.query;
    try {
        let personalDetails = [];
        if (viewAll === 'true' ) {
            personalDetails = await PersonalData.find().populate('educations');
        } else {
            personalDetails = await PersonalData.find();
        }

        return res.status(200).json(personalDetails)
    } catch (error) {
        return next(error)
    }
});


router.post('/', async (req, res, next) => {
    const { name, lastName, birth, country, city, comments, photo } = req.body;
    const personalData ={
        name,
        lastName,
        birth,
        country,
        city,
        comments,
        photo,
    };    
    try {
        const newPersonalData = new PersonalData(personalData);
        const createdPersonalData = await newPersonalData.save();
        return res.status(201).json(createdPersonalData);
    } catch (error) {
        next(error);
    }
})

router.delete('/:id', async (req, res, next) => {
    try {
        const { id } = req.params;
        await PersonalData.findByIdAndDelete(id);
        return res.status(200).json('Personal Data deleted!');
    } catch (error) {
        return next(error);
    }
});

router.put('/:id', async (req, res, next) =>{
    try{
        const { id } = req.params;
        const personalDataEdited = new PersonalData(req.body);
        personalDataEdited._id = id;
        await PersonalData.findByIdAndUpdate(id, personalDataEdited);
        return res.status(200).json(personalDataEdited)
    } catch (error) {
        return next(error)
    }
})

router.post('/:personalDataId/addEducation', async (req, res, next) => {
    try {
        const { personalDataId } =req.params;
        const { educationId } = req.body;
        const personalDataUpdated = await PersonalData.findByIdAndUpdate(
            personalDataId,
            { $push: { educations: educationId } },
            { new: true }
        );
        return res.status(200).json(personalDataUpdated);
        
    } catch (error) {
            return next(error);    
    }
})
router.post('/:personalDataId/addWorkexperience', async (req, res, next) => {
    try {
        const { personalDataId } =req.params;
        const { workExperienceId } = req.body;
        const personalDataUpdated = await PersonalData.findByIdAndUpdate(
            personalDataId,
            { $push: { workExperience: workExperienceId } },
            { new: true }
        );
        return res.status(200).json(personalDataUpdated);
        
    } catch (error) {
            return next(error);    
    }
})

router.post('/:personalDataId/addCertification', async (req, res, next) => {
    try {
        const { personalDataId } =req.params;
        const { certificationsId } = req.body;
        const personalDataUpdated = await PersonalData.findByIdAndUpdate(
            personalDataId,
            { $push: { certifications: certificationsId } },
            { new: true }
        );
        return res.status(200).json(personalDataUpdated);
        
    } catch (error) {
            return next(error);    
    }
})

router.post('/:personalDataId/addPortfolio', async (req, res, next) => {
    try {
        const { personalDataId } =req.params;
        const { portfolioId } = req.body;
        const personalDataUpdated = await PersonalData.findByIdAndUpdate(
            personalDataId,
            { $push: { portfolio: portfolioId } },
            { new: true }
        );
        return res.status(200).json(personalDataUpdated);
        
    } catch (error) {
            return next(error);    
    }
})





module.exports = router;
