const express = require('express');

const PortFolio = require('../models/Portfolio');

const router = express.Router();


router.get('/', async (req, res, next) => {
    try {
        const portfolio = await PortFolio.find();
        return res.status(200).json(portfolio)
    } catch (error) {
        return next(error)
    }
});

router.post('/', async (req, res, next) => {
    const { title, link, photo, details } = req.body;
    const portfolio ={
        title,
        link,
        photo,
        details,
    };    
    try {
        const newPortfolio = new PortFolio(portfolio);
        const createdPortfolio = await newPortfolio.save();
        return res.status(201).json(createdPortfolio);
    } catch (error) {
        next(error);
    }
})

module.exports = router;