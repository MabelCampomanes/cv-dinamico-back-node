const express = require('express');

const WorkExperience = require('../models/WorkExperience');

const router = express.Router();


router.get('/', async (req, res, next) => {
    try {
        const workExperience = await WorkExperience.find();
        return res.status(200).json(workExperience)
    } catch (error) {
        return next(error)
    }
});

router.post('/', async (req, res, next) => {
    const { rol, company, startDate, endDate, rolDescription } = req.body;
    const workExperience ={
        rol,
        company,
        startDate,
        endDate,
        rolDescription
    };    
    try {
        const newWorkExperience = new WorkExperience(workExperience);
        const createdWorkExperience = await newWorkExperience.save();
        return res.status(201).json(createdWorkExperience);
    } catch (error) {
        next(error);
    }
})

module.exports = router;