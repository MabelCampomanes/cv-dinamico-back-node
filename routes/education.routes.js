const express = require('express');
const mongoose = require('mongoose');

const Education = require('../models/Education');

const router = express.Router();


router.get('/', async (req, res, next) => {
    try {
        const education = await Education.find();
        return res.status(200).json(education)
    } catch (error) {
        return next(error)
    }
});

router.post('/', async (req, res, next) => {
    const { title, school, startDate, endDate } = req.body;
    const education ={
        title,
        school,
        startDate,
        endDate,
    };    
    try {
        const newEducation = new Education(education);
        const createdEducation = await newEducation.save();
        return res.status(201).json(createdEducation);
    } catch (error) {
        next(error);
    }
})

module.exports = router;