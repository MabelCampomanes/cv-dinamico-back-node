const express = require('express');

const Certification = require('../models/Certifications');

const router = express.Router();


router.get('/', async (req, res, next) => {
    try {
        const certification = await Certification.find();
        return res.status(200).json(certification)
    } catch (error) {
        return next(error)
    }
});

router.post('/', async (req, res, next) => {
    const { title, school, startDate, endDate } = req.body;
    const certification ={
        title,
        school,
        startDate,
        endDate
    };    
    try {
        const newCertification = new Certification(certification);
        const createdCertification = await newCertification.save();
        return res.status(201).json(createdCertification);
    } catch (error) {
        next(error);
    }
})

module.exports = router;