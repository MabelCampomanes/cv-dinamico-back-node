const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const personalDataSchema = new Schema(
  {
    name: { type: String, required: true },
    lastName: { type: String, required: true },
    birth: { type: Date },
    country: { type: String },
    city: { type: String },
    comments: { type: String },
    photo: { type: String },
    educations: [{ type: mongoose.Types.ObjectId, ref: 'Education' }],
    workExperience: [{ type: mongoose.Types.ObjectId, ref: 'WorkExperiences'}],
    certifications: [{ type: mongoose.Types.ObjectId, ref: 'Certifications'}],
    portfolio: [{ type: mongoose.Types.ObjectId, ref: 'Portfolios'}],
  },
  {
    timestamps: true,
    collection: 'personaldatas'
  }
);

const PersonalData = mongoose.model('PersonalData', personalDataSchema);
module.exports = PersonalData;
