const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const certificationsSchema = new Schema(
  {
    title: { type: String, required: true },
    school: { type: String, required: true },
    startDate: { type: Date },
    endDate: { type: String},
  },
  {
    timestamps: true,
  }
);

const Certification = mongoose.model('Certifications', certificationsSchema);
module.exports = Certification;