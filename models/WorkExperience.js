const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const workExperienceSchema = new Schema(
  {
    rol: { type: String, required: true },
    company: { type: String, required: true },
    country:{ type: String },
    startDate: { type: Date },
    endDate: { type: String},
    rolDescription: { type: String},
  },
  {
    timestamps: true,
  }
);

const WorkExperience = mongoose.model('WorkExperience', workExperienceSchema);
module.exports = WorkExperience;