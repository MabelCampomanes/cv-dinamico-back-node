const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const educationSchema = new Schema(
  {
    title: { type: String, required: true },
    school: { type: String, required: true },
    startDate: { type: Date },
    endDate: { type: String},
  },
  {
    timestamps: true,
    collection: 'educations'
  }
);

const Education = mongoose.model('Education', educationSchema);
module.exports = Education;