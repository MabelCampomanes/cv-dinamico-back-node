const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const portfolioShema = new Schema(
  {
    title: { type: String, required: true },
    link: { type: String },
    photo: { type: Date },
    details: { type: String},
  },
  {
    timestamps: true,
  }
);

const PortFolio = mongoose.model('Portfolio', portfolioShema);
module.exports = PortFolio;