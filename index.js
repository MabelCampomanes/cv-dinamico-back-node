const express = require('express');

// Routes
const personalDataRoutes = require('./routes/personal.routes.js');
const educationRoutes = require('./routes/education.routes.js');
const workExperienceRoutes = require('./routes/workExperience.routes.js');
const certiciationRoutes = require('./routes/certifications.routes.js');
const portfolioRoutes = require('./routes/portfolio.routes.js');

// Datbase
require('./utils/db.js');

// Server Config
const PORT = 3000;
const server = express();

// Middlewares
server.use(express.json());
server.use(express.urlencoded({extended: true}));

// Routes
const router = express.Router();

// Routes middleware
server.use('/personalData', personalDataRoutes);
server.use('/education', educationRoutes);
server.use('/workExperience', workExperienceRoutes);
server.use('/certifications', certiciationRoutes);
server.use('/portfolio', portfolioRoutes);
server.use('/', router);

server.use((err, req, res, next) => {
  console.error(err); // Opcional: imprimir el error en la consola para fines de depuración
  res.status(500).json('Internal Server Error');
});

server.listen(PORT, () => {
  console.log(`Server running in http://localhost:${PORT}`);
});