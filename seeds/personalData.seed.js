const mongoose = require('mongoose');
const PersonalData = require('../models/PersonalData');

const personalDataList = [
  {
    name: "Mabel",
    lastName: "Campomanes Isidoro",
    country: "España",
    city: "Alcalá de Henares",
    comments: "",
  },
  {
    name: "Carlos",
    lastName: "Gutiérrez Sánchez",
    country: "España",
    city: "Alcalá de Henares",
    comments: "",
  }
];

const personalDataDocuments = personalDataList.map(item => new PersonalData(item));

mongoose
  .connect('mongodb+srv://mabel:050920@cluster0.pjgjkwp.mongodb.net/cv-dinamico-node', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
    const allPersonalData = await PersonalData.find();
    if (allPersonalData.length) {
      await PersonalData.collection.drop();
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))
  .then(async () => {
    await PersonalData.insertMany(personalDataDocuments);
    console.log("Data inserted successfully!");
  })
  .catch((err) => console.log(`Error creating data: ${err}`))
  .finally(() => mongoose.disconnect());